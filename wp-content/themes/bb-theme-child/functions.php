<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
  //  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/jquery.mask.js","","",1);
});

// Disable Updraft and Wordfence on localhost
add_action( 'init', function () {
    if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );
       
    }
    register_shortcodes();
} );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_filter('wp_nav_menu_items', 'do_shortcode');

//add_filter( 'gform_validation', 'custom_validation' );
function custom_validation( $validation_result ) {
    $form = $validation_result['form'];
 
    //supposing we don't want input 1 to be a value of 86
    if ( empty($_POST["marketing[]"]) ) {
 
        // set the form validation to false
        $validation_result['is_valid'] = false;
 
        //finding Field with ID of 1 and marking it as failed validation
        foreach( $form['fields'] as &$field ) {
 
            //NOTE: replace 1 with the field you would like to validate
            if ( $field->name == 'marketing[]' ) {
                $field->failed_validation = true;
                $field->validation_message = 'This field is invalid!';
                break;
            }
        }
 
    }
 
    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
 
}

// The shortcode function
function digital_package_function() { 
 
    // Advertisement code pasted inside a variable
    $string = '';
    if($_POST['input_50']=='VELOCITYPLATINUM_1700_3500'){

        $string = 'VELOCITY PLATINUM';

    }elseif($_POST['input_50']=='VELOCITYPREMIUM_1150_2900') {
        
        $string = 'VELOCITY PREMIUM';

    }elseif($_POST['input_50']=='VELOCITYPRIME_750_1700') {
        
        $string = 'VELOCITY PRIME';

    }
     
    // Ad code returned
    return $string; 
     
    }
    // Register shortcode
    add_shortcode('digitalpackage', 'digital_package_function'); 

    function digital_package_amount_month_function() { 
 
        // Advertisement code pasted inside a variable
        $monthstring = '';
        $setupstring = '';
        if($_POST['input_50']=='VELOCITYPLATINUM_1700_3500'){
    
            $monthstring = '1700';
    
        }elseif($_POST['input_50']=='VELOCITYPREMIUM_1150_2900') {
            
            $monthstring = '1150';
    
        }elseif($_POST['input_50']=='VELOCITYPRIME_750_1700') {
            
            $monthstring = '750';
    
        }

        if($_POST['input_50']=='VELOCITYPLATINUM_1700_3500'){
    
            $setupstring = '3500';
    
        }elseif($_POST['input_50']=='VELOCITYPREMIUM_1150_2900') {
            
            $setupstring = '2900';
    
        }elseif($_POST['input_50']=='VELOCITYPRIME_750_1700') {
            
            $setupstring = '1700';
    
        }
         
        // Ad code returned
        $html_pack = '<div class="monthlySub"><h2>$'.$monthstring.'<span>/month</span></h2></div>
                     <hr class="divider">
                    <div class="monthlySetup"><h2>$'.$setupstring.'<span>setup</span></h2></div>';
        return $html_pack; 
         
        }
        // Register shortcode
        add_shortcode('digitalpackage_monthly_setup', 'digital_package_amount_month_function');     
      

    function advertising_package_amount_month_function() { 
 
        // Advertisement code pasted inside a variable
        $monthstring = '';
        $setupstring = '';
        if($_POST['input_51']=='250_100'){
    
            $monthstring = '250';
    
        }elseif($_POST['input_51']=='500_250') {
            
            $monthstring = '500';
    
        }elseif($_POST['input_51']=='1000_400') {
            
            $monthstring = '1000';
    
        }

        if($_POST['input_51']=='250_100'){
    
            $setupstring = '100';
    
        }elseif($_POST['input_51']=='500_250') {
            
            $setupstring = '250';
    
        }elseif($_POST['input_51']=='1000_400') {
            
            $setupstring = '400';
    
        }
         
        // Ad code returned
        $html_pack = '<h3>$'.$monthstring.' / MONTH</h3><span> +$'.$setupstring.' setup fee</span>';
        return $html_pack; 
         
        }
        // Register shortcode
        add_shortcode('advertising_monthly_setup', 'advertising_package_amount_month_function'); 
      
        add_filter( 'gform_authorizenet_transaction_pre_capture', 'add_custom_field', 10, 5 );
            function add_custom_field( $transaction, $form_data, $config, $form, $entry ) {
                if ( $form['id'] == 19 ) {
                    $value = rgpost( 'input_17');
                    $transaction->setCustomField( 'company_name', $value );
                }
            
                return $transaction;
            }
        
